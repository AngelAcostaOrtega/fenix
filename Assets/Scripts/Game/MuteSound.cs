using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteSound : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        this.GetComponent<AudioSource>().mute = PlayerPrefs.GetInt("Sound") == 0 ? false : true;
    }
}
