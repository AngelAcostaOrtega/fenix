using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pocion : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(GameObject.FindGameObjectWithTag("Player").GetComponent<PjLifes>().lifes < 3)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PjLifes>().sumarVida();
            Destroy(this.gameObject, 0.01f);
        }
    }
}
