using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField][Range(0, 2)]
    private float velocity;
    [HideInInspector]
    public float x;

    // Update is called once per frame
    void Update()
    {
        //x = Input.GetAxis("Horizontal");
        if (x != 0)
        {
            this.transform.Translate(Vector2.right * -x * velocity * Time.deltaTime);
        }
    }
}
