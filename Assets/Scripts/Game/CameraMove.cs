﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    //[SerializeField][Range(0, 100)]private float cameraspeed;
    private Transform pp;
    private Rigidbody2D prb;
    private GameObject[] fondos;
    private Vector3 posb;
    private void Start()
    {
        pp = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        prb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        fondos = GameObject.FindGameObjectsWithTag("Parallax");
        
    }
    void Update()
    {
        this.TryGetComponent(out Rigidbody2D cp);

        Vector2 limitemaximo = Camera.main.ScreenToWorldPoint( (Vector3)(new Vector2(
                                                                        ((Camera.main.scaledPixelWidth / 6) * 3.5f),
                                                                        ((Camera.main.scaledPixelHeight / 6) * 6f))) );
        Vector2 limiteminimo = Camera.main.ScreenToWorldPoint( (Vector3)(new Vector2(
                                                                        ((Camera.main.scaledPixelWidth / 6) * 2),
                                                                        ((Camera.main.scaledPixelHeight / 6) * 2))) );
        float direction = 0;
        if(pp.position.x > limitemaximo.x)
        {
            direction = (prb.velocity.x >= 0) ? prb.velocity.x : prb.velocity.x * -1;
            //(Vector2.right * direction * Time.deltaTime);
            cp.velocity = new Vector2(direction, cp.velocity.y);

            if(!this.transform.position.Equals(posb))
            {
                parallax(1f);
                posb = this.transform.position;
            } else parallax(0f);
        }
        else if(pp.position.x < limiteminimo.x)
        {
            direction = (prb.velocity.x >= 0) ? prb.velocity.x : prb.velocity.x * -1;
            //cp.velocity = (Vector2.left * direction * Time.deltaTime);
            cp.velocity = new Vector2(-direction, cp.velocity.y);

            if (!this.transform.position.Equals(posb))
            {
                parallax(-1f);
                posb = this.transform.position;
            } else parallax(0f);

        }
        else if(cp.velocity.x > 0f|| cp.velocity.x < 0)
        {
            cp.velocity = Vector2.zero;

            parallax(0f);
        }

        if(pp.position.y > limitemaximo.y)
        {
            direction = (prb.velocity.y >= 0) ? prb.velocity.y : prb.velocity.y * -1;
            cp.velocity = (Vector2.up * direction * Time.deltaTime);
        }
        else if(pp.position.y < limiteminimo.y)
        {
            direction = (prb.velocity.y >= 0) ? prb.velocity.y : prb.velocity.y * -1;
            cp.velocity = (Vector2.down * direction * Time.deltaTime);
        }

    }

    private void parallax(float i)
    {
        for (int n = 0; n < fondos.Length; n++)
        {
            fondos[n].GetComponent<Parallax>().x = i;
        }
    }
}
