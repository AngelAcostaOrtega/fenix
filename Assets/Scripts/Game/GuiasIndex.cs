using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiasIndex : MonoBehaviour
{
    [HideInInspector]
    public int number;
    private void Start()
    {
        number = int.Parse(this.gameObject.name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            GameObject g = GameObject.Find("PlumaGuia");
            if (this.transform.position.Equals(g.transform.position))
            {
                g.GetComponent<MovimientoPluma>().sumar();
            }
        }
    }
}
