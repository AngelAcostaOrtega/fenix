using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lampara : MonoBehaviour
{
    private float a, b;
    private float actual;
    private bool flag;
    [SerializeField][Range(0, 1)]
    private float velocity;
    private void Start()
    {
        a = 0.001f;
        b = 0.1f;
        if(velocity == 0.0f) velocity = 0.1f;
    }
    // Update is called once per frame
    void Update()
    {
        this.gameObject.TryGetComponent(out SpriteMask sm);

        actual += (flag ? velocity : -velocity) * Time.deltaTime;
        if (actual > b) flag = false;
        if (actual < a) flag = true;

        sm.alphaCutoff = actual;
    }
}
