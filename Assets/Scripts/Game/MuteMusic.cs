using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteMusic : MonoBehaviour
{
    void Update()
    {
        this.GetComponent<AudioSource>().mute = PlayerPrefs.GetInt("Music") == 0 ? false : true;
    }
}
