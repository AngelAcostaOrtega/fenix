using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPluma : MonoBehaviour
{
    private GameObject[] guias;
    private int pos;

    private bool flag;
    private float crono;

    void Start()
    {
        guias = GameObject.FindGameObjectsWithTag("Guias");
        ordenar(guias);
        pos = 0;

        flag = false;
        crono = 0.0f;
    }

    void Update()
    {
        if(pos < guias.Length)
        {
            if (this.transform.position != guias[pos].transform.position)
            {
                if(Vector2.Distance(this.transform.position, guias[pos].transform.position) > 0.1f)
                {
                    float velocityRate = 10f / Vector2.Distance(this.transform.position, guias[pos].transform.position);
                    this.transform.position = Vector3.Lerp(this.transform.position, guias[pos].transform.position, velocityRate * Time.deltaTime );
                }
                else
                {
                    this.transform.position = guias[pos].transform.position;
                }
            }
        }
        else if(pos > guias.Length)
        {
            if(flag)
            {
                crono += Time.deltaTime;
                if (crono >= 4f) Camera.main.GetComponent<GameMotor>().win();
            }
        }
    }

    public void sumar()
    {
        this.pos++;
        if (pos > guias.Length)
        {
            this.transform.Find("Win").GetComponent<ParticleSystem>().Play();

            Destroy(GameObject.FindGameObjectWithTag("Player"), 0.001f);

            flag = true;
        }
    }

    private void ordenar(GameObject[] list)
    {
        int n = 0;
        GameObject[] nueva = new GameObject[list.Length-1];

        
        for(int i = 0; i < list.Length; i++)
        {
            for (int j = 1; j < list.Length; j++)
            {
                list[j].gameObject.TryGetComponent(out GuiasIndex gi);
                if (gi != null)
                {
                    if (gi.number == n+1)
                    {
                        nueva[n] = list[j];
                        n++;
                        break;
                    }
                }
            }
        }

        this.guias = nueva;
    }
}
