﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LvlMng : MonoBehaviour
{
    public void sceneChange(int n)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(n);
    }

    public void sceneChange(string n)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(n);
    }

    public void restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void salir()
    {
        Application.Quit();
    }
    public void nextStage()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
