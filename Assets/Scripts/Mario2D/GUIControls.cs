﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GUIControls : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    //Evento al pasar por encima del boton
    public void OnPointerDown(PointerEventData ped)
    {
        GameObject.FindGameObjectWithTag("Player").TryGetComponent(out Jugador j);
        if (this.gameObject.name.Equals("BLeft") || this.gameObject.name.Equals("BRight"))
        {
            int n = this.gameObject.name.Equals("BLeft") ? -1 : 1;
            j.x = n;
        }
        else if(this.gameObject.name.Equals("BRun"))
        {
            j.shift = true;
        }
        else if (this.gameObject.name.Equals("BJump"))
        {
            j.space = true;
        }
    }

    //Evento al pulsar el boton
    public void OnPointerUp(PointerEventData ped)
    {
        GameObject.FindGameObjectWithTag("Player").TryGetComponent(out Jugador j);
        if (this.gameObject.name.Equals("BLeft") || this.gameObject.name.Equals("BRight"))
        {
            j.x = 0;
        }
        else if (this.gameObject.name.Equals("BRun"))
        {
            j.shift = false;
        }
        else if (this.gameObject.name.Equals("BJump"))
        {
            j.space = false;
        }
    }

}
