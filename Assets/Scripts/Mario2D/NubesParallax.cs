﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NubesParallax : MonoBehaviour
{

    void Update()
    {
        this.transform.Translate(Vector2.left * Time.deltaTime / 2);
        
        if(this.transform.localPosition.x <= -25.0f)
        {
            this.transform.localPosition = new Vector3(20f, this.transform.localPosition.y, this.transform.localPosition.z);
        }
    }
}
