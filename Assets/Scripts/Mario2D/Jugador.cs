﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Jugador : MonoBehaviour
{
    public int velocidad;
    [HideInInspector]public float x;
    public int vida;

    private bool tictac;
    private float tempo;
    private float crono;
    private float trotar;

    private bool saltando;
    private bool doublejump;
    private bool jump;
    [SerializeField][Range(1, 4)]private float JumpPower;

    private bool rapid;

    private AudioSource[] speakers;

    //Controles manuales
    [HideInInspector] public bool shift;
    [HideInInspector] public bool space;

    // Start is called before the first frame update
    void Start()
    {
        crono = 0.3f;
        trotar = 0.5f;
        tempo = 0.0f;
        saltando = false;
        jump = true;
        doublejump = true;
        rapid = false;

        if (JumpPower == 0) JumpPower = 1.6f;
        if(vida == 0) vida = 3;
        speakers = this.gameObject.GetComponents<AudioSource>();
        Camera.main.GetComponent<GameMotor>().cambiaVida(vida);

        shift = false;
        space = false;
    }

    // Update is called once per frame
    void Update()
    {
        this.TryGetComponent(out Rigidbody2D rb);
        this.TryGetComponent(out SpriteRenderer sr);
        //this.TryGetComponent(out Animator an);
        if(Input.GetKey(KeyCode.A)||Input.GetKey(KeyCode.D)) x = Input.GetAxisRaw("Horizontal");
        

        if (x != 0)
        {
            float multi;
            if (Input.GetKey(KeyCode.LeftShift) || shift)
            {
                multi = 1.25f;
            }
            else
            {
                multi = 1f;
            }
            rb.velocity = new Vector2(x * (rapid ? velocidad * 1.4f : velocidad) * multi * Time.deltaTime / 1.5f,// (an.GetBool("saltando") ? 1.5f:1f),
                                      rb.velocity.y); ;
            
            sr.flipX = x < 0 ? true : false;

            //an.SetBool("moviendose", true);
            //an.SetBool( "corriendo", (multi > 1f ? true : false) );
            if(jumpcollisions(Vector2.right * x))
            {
                rb.velocity = new Vector2(0, rb.velocity.y);
            }
            
        }
        /*else if(!an.GetBool("saltando") )
        {
            rb.velocity = new Vector2(rb.velocity.x + ( rb.velocity.x == 0 ? 0:(rb.velocity.x < 0 ? 0.1f:-0.1f) ), rb.velocity.y);
            an.SetBool("moviendose", false);
        }*/
        
        //Primer salto
        if ( (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.W) || space) && !tictac && jump /*&& an.GetInteger("saltos") == 0*/)
        {
            if(rb.velocity.y <= 0.1f )//&& !an.GetBool("saltando"))
            {
                speakers[0].Play();
                rb.AddRelativeForce(new Vector2(0,
                                                500 * 1.25f) * 0.014f,
                                                ForceMode2D.Impulse);

                //an.SetBool("saltando", true);
                saltando = true;
            }
            else if(saltando)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + 0.5f);
                saltando = rb.velocity.y <= ( 500 * 1.25f * (JumpPower/100) ) ? true : false;
            }
        }
        //Segundo salto
        else if((Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.W) || space) && tictac &&  tempo >= 0.1f && jump && doublejump)
        {
            if (rb.velocity.y <= 0.1f )//&& !an.GetBool("saltando"))
            {
                speakers[0].Play();
                rb.AddRelativeForce(new Vector2(0,
                                                500 * 1.50f) * 0.014f,
                                                ForceMode2D.Impulse);

                //an.SetBool("saltando", true);
                //an.SetInteger("saltos", 1);
                saltando = true;
            }
            else if (saltando && tictac)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + 0.8f);
                saltando = rb.velocity.y <= (500 * 1.50f * (JumpPower / 100)) ? true : false;
                if (tictac && !saltando) tictac = false;
            }
        }

        if( /*an.GetBool("saltando") &&*/ rb.velocity.y <= 0.1f )
        {
            jump = false;
            //an.SetBool("cayendo", true);
            //grounded(an);
        }

        /*  
         *  V Cronometros del movimiento V
         */

        if (tictac)
        {
            tempo += Time.deltaTime;
            if(tempo >= crono)
            {
                tictac = false;
                tempo = 0.0f;
                //an.SetInteger("saltos", 0);
                if (!jump) jump = true;
                if (!doublejump) doublejump = true;
            }
        }
        if (/*an.GetBool("moviendose") &&*/ !rapid)
        {
            tempo += Time.deltaTime;
            if(tempo >= trotar)
            {
                rapid = true;
                tempo = 0.0f;
                //an.SetBool("andadorapido", true);
            }
        }
        else if(/*!an.GetBool("moviendose") &&*/ rapid)
        {
            tempo += Time.deltaTime;
            if(tempo >= crono)
            {
                rapid = false;
                tempo = 0.0f;
                //an.SetBool("andadorapido", false);
            }
        }
    }
    public void OnCollisionEnter2D(Collision2D col)
    {
        this.TryGetComponent(out Animator an);
        this.TryGetComponent(out Rigidbody2D rb);

        if (an.GetBool("saltando"))
        {
            rb.velocity = (Vector3)(new Vector2(rb.velocity.x, 0));
            jump = false;
        }
    }
    public void grounded(Animator an)
    {
        Vector2 pos = new Vector2(this.transform.position.x,
                                  this.transform.position.y - (this.GetComponent<BoxCollider2D>().bounds.extents.y * 1.5f) );
        if(Physics2D.Raycast(pos, Vector2.down, 0.1f))
        {
            an.SetBool("cayendo", false);
            an.SetBool("saltando", false);

            tictac = true;
            tempo = 0.0f;
            jump = true;
            if (an.GetInteger("saltos") != 0) doublejump = false;
        }
    }
    public bool jumpcollisions(Vector2 direction)
    {
        this.TryGetComponent(out BoxCollider2D bc);

        Vector2 pos = Vector2.zero;
        float longitud = 0.1f;
        if (direction.x != 0 && direction.y == 0)
        {
            pos = new Vector2(this.transform.position.x + ((bc.bounds.extents.x + 0.1f) * direction.x),
                                      this.transform.position.y + ((bc.bounds.extents.y) * 1));
            direction = Vector2.down;
            longitud = bc.bounds.size.y;
        }
        else if (direction.y != 0 && direction.x == 0)
        {
            pos = new Vector2(this.transform.position.x + ((bc.bounds.extents.x) * -1) ,
                              this.transform.position.y + ((bc.bounds.extents.y + 0.1f) * direction.y) );
            direction = Vector2.right;
            longitud = bc.bounds.size.x;
        }
        if (Physics2D.Raycast(pos, direction, longitud))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void restarVida()
    {
        vida--;
        Camera.main.GetComponent<GameMotor>().cambiaVida(vida);
        if(vida <= 0)
        {
            Camera.main.GetComponent<GameMotor>().lose();
            Destroy(this.gameObject, 0.01f);
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name.Equals("meta"))
        {
            Camera.main.GetComponent<GameMotor>().win();
        }
    }
}
