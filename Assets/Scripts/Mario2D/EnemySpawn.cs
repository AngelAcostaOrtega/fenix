﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    [SerializeField]private GameObject enemy;
    private float tempo;
    [SerializeField]private float SpawnTime;
    void Start()
    {
        if (enemy == null) enemy = null;
        if (SpawnTime <= 0) SpawnTime = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        tempo += Time.deltaTime;
        if(tempo >= SpawnTime)
        {
            Instantiate<GameObject>(enemy, this.transform.position ,Quaternion.identity);
            tempo = 0.0f;
        }
    }
}
