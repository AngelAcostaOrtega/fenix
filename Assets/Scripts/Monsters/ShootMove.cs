using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootMove : MonoBehaviour
{
    [HideInInspector]
    public float direccion;

    private void Start()
    {
        Destroy(this.gameObject, 4f);
    }
    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Translate(new Vector2(direccion * 10 * Time.deltaTime, 0));
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.CompareTag("Player"))
        {
            col.gameObject.TryGetComponent(out PjLifes pl);
            pl.restarVida();
        }
        Destroy(this.gameObject, 0.01f);
    }
}
