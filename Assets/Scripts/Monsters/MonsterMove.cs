using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMove : MonoBehaviour
{
    [SerializeField]
    private float a, b;
    private Vector2 A, B;

    // Start is called before the first frame update
    void Start()
    {
        A = new Vector2(this.transform.position.x - a, this.transform.position.y);
        B = new Vector2(this.transform.position.x + b, this.transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject pj = GameObject.FindGameObjectWithTag("Player");
        if(Vector2.Distance(this.transform.position, pj.transform.position) < 5)
        {
            this.transform.Translate((pj.transform.position - this.transform.position) * Time.deltaTime);
        }
    }
}
