using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapLock : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Player"))
        {
            Destroy(this.gameObject, 0.01f);
        }
    }
}
