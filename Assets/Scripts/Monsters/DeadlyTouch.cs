using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadlyTouch : MonoBehaviour
{
    private float n;
    private bool flag;
    private void Update()
    {
        if (flag)
        {
            if (n > 1f)
            {
                n = 0f;
                flag = false;
            }
            else n += Time.deltaTime;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if (!flag)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<PjLifes>().restarVida();
                flag = true;
            }
        }
    }
}
