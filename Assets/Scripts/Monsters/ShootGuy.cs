using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootGuy : MonoBehaviour
{
    [SerializeField]
    private GameObject disparo;
    [SerializeField]
    private bool izquierda;

    private float crono;

    // Start is called before the first frame update
    void Start()
    {
        crono = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        crono += Time.deltaTime;
        if(crono >= 4.0f)
        {
            shoot();
            crono = 0.0f;
        }
    }

    public void shoot()
    {
        Vector2 pos = GameObject.Find(this.gameObject.name.ToString() + "/ShotPoint").transform.position;
        disparo.TryGetComponent(out ShootMove sm);
        sm.direccion = izquierda ? -1:1;
        Instantiate(disparo, this.transform);
    }
}
