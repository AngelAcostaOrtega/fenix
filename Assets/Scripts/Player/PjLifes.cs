using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PjLifes : MonoBehaviour
{
    [HideInInspector]
    public int lifes;
    private bool flag;
    private float crono;
    // Start is called before the first frame update
    void Start()
    {
        if (lifes == 0) lifes = 3;
        flag = false;
    }

    private void Update()
    {
        if(lifes != PlayerPrefs.GetInt("Lifes")) PlayerPrefs.SetInt("Lifes", lifes);
        flag = lifes > 0 ? false : true;
        if(flag)
        {
            this.gameObject.TryGetComponent(out PlayerMove pm);
            pm.move = false;
            crono += Time.deltaTime;
            this.GetComponent<SpriteRenderer>().enabled = false;
            this.transform.Find("DParticles").GetComponent<ParticleSystem>().Play();
            if (crono >= 2f) dead();
        }
    }

    public void restarVida()
    {
        lifes--;
    }
    private void dead()
    {
        Camera.main.GetComponent<GameMotor>().lose();
        PlayerPrefs.SetInt("Deaths", PlayerPrefs.GetInt("Deaths") + 1);
        Destroy(this.gameObject, 2f);
    }
    public void sumarVida()
    {
        lifes++;
    }
}
