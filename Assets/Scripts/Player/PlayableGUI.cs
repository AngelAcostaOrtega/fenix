using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableGUI : MonoBehaviour
{
    [SerializeField]
    private GameObject GUI;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject g = GUI;
            if (!g.activeSelf)
            {
                Time.timeScale = 0;
                g.SetActive(true);
                if(!GameObject.Find("Canvas/Papiro/Pause").activeSelf)
                {
                    GameObject.Find("Canvas/Papiro/Pause").SetActive(true);
                    if(GameObject.Find("Canvas/Papiro/Options").activeSelf)
                    {
                        GameObject.Find("Canvas/Papiro/Options").SetActive(false);
                    }
                }
            }
            else
            {
                Time.timeScale = 1;
                g.SetActive(false);
            }
        }
    }
}
