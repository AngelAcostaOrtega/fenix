using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    private float x, y;
    [SerializeField][Range(1, 10)]
    private int velocidad;

    private bool salto, counting;
    private float crono;
    [HideInInspector]
    public bool move;

    // Start is called before the first frame update
    void Start()
    {
        if (velocidad == 0) velocidad = 1;
        salto = false;
        move = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (move)
        {
            this.TryGetComponent(out Rigidbody2D rb);

            x = Input.GetAxis("Horizontal");
            y = Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow) ? 1f : 0f;

            if (x != 0)
            {
                this.GetComponent<SpriteRenderer>().flipX = x < 0 ? true : false;
                rb.velocity = new Vector2((x * velocidad * 20 * Time.deltaTime), rb.velocity.y);
                

                if (walled(Vector2.right * (x > 0 ? 1f:-1f))) rb.velocity = new Vector2(0f, rb.velocity.y);
            }

            if (salto) grounded();
            else if(y > 0)
            {
                salto = true;
                if (salto) grounded();
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce( new Vector2(0, y * velocidad * 15 * rb.mass * 0.048f), ForceMode2D.Impulse);
            }
        }
        else
        {
            this.TryGetComponent(out PjLifes pl);
            if(pl.lifes > 0)
            {
                crono += Time.deltaTime;
                if (crono >= 1f) move = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Trap"))
        {
            x = 0f;
            y = 0f;


            this.TryGetComponent(out PjLifes pl);
            pl.lifes = 0;
            move = false;
            this.TryGetComponent(out Rigidbody2D rb);
            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(0, 1f * velocidad * 10 * rb.mass * (Time.deltaTime * 2)), ForceMode2D.Impulse);

            this.TryGetComponent(out Collider2D c);
            Destroy(c, 0.01f);
            Destroy(this.gameObject, 3f);
        }
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.CompareTag("Shoot"))
        {
            move = false;

            this.TryGetComponent(out Rigidbody2D rb);
            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(0, 1f * velocidad * 10 * rb.mass * (Time.deltaTime * 2)), ForceMode2D.Impulse);
        }
    }

    public void grounded()
    {
        Vector2 pos = new Vector2(this.transform.position.x - (this.GetComponent<BoxCollider2D>().bounds.extents.x * 0.98f),
                                  this.transform.position.y - (this.GetComponent<BoxCollider2D>().bounds.extents.y * 1.01f));
        if (Physics2D.Raycast(pos, Vector2.down, 0.1f) )
        {
            salto = false;
        }
        else
        {
            pos.x *= -1f;
            if (Physics2D.Raycast(pos, Vector2.down, 0.1f))
            {
                salto = false;
            }
        }
    }

    private bool walled(Vector2 direction)
    {
        this.TryGetComponent(out BoxCollider2D bc);

        Vector2 pos = Vector2.zero;
        float longitud = 0.1f;
        if (direction.x != 0 && direction.y == 0)
        {
            pos = new Vector2(this.transform.position.x + ((bc.bounds.extents.x + 0.3f) * direction.x),
                                      this.transform.position.y + ((bc.bounds.extents.y) * 1));
            direction = Vector2.down;
            longitud = bc.bounds.size.y - 0.01f;
        }
        else if (direction.y != 0 && direction.x == 0)
        {
            pos = new Vector2(this.transform.position.x + ((bc.bounds.extents.x) * -1),
                              this.transform.position.y + ((bc.bounds.extents.y + 0.1f) * direction.y));
            direction = Vector2.right;
            longitud = bc.bounds.size.x;
        }
        if (Physics2D.Raycast(pos, direction, longitud))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
