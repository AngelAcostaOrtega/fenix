using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animations : MonoBehaviour
{
    private bool jumping, walking, flag;

    // Start is called before the first frame update
    void Start()
    {
        jumping = false;
        walking = false;
    }

    // Update is called once per frame
    void Update()
    {
        this.TryGetComponent(out Animator anim);
        if(Input.GetAxis("Horizontal") != 0)
        {
            if (this.GetComponent<PlayerMove>().move) walking = true;
            else walking = false;
        }
        else
        {
            walking = false;
        }

        if(!grounded())
        {
            jumping = true;
        }
        else
        {
            jumping = false;
        }

        anim.SetBool("walk", walking);
        anim.SetBool("jump", jumping);
    }

    public bool grounded()
    {
        Vector2 pos = new Vector2(this.transform.position.x,
                                  this.transform.position.y - (this.GetComponent<BoxCollider2D>().bounds.extents.y * 1.01f));
        if (Physics2D.Raycast(pos, Vector2.down, 0.01f))
        {
            return true;
        } else return false;
    }
}
