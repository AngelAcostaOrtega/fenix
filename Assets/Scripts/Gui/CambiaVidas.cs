using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CambiaVidas : MonoBehaviour
{
    [SerializeField][Range(1, 3)]
    private int vidas;
    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Image>().enabled = PlayerPrefs.GetInt("Lifes") >= vidas ? true : false;
    }
}
