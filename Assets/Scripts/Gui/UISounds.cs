﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UISounds : MonoBehaviour
{

    public Transform padre;
    public Transform tio;
    private float tiempo;
    private float t;

    private bool counting;
    private bool cont;
    private bool cambiar;
    private void Start()
    {
        counting = false;
        cont = false;
        cambiar = false;
        t = 0.0f;
        tiempo = 0.0f;
    }

    //Update que va contando el tiempo que pasa desde que inicia la cuenta atras hasta que se para y vuelta.
    private void Update()
    {
        if (counting)
        {
            t += Time.unscaledDeltaTime;
            if (t >= tiempo && cambiar)
            {
                cambiar = false;
                counting = false;
                t = 0;
                tiempo = 0;

                tio.gameObject.SetActive(true);
                padre.gameObject.SetActive(false);
            }
            if (t >= tiempo && cont)
            {
                cont = false;
                counting = false;
                t = 0;
                tiempo = 0;


                Time.timeScale = 1.0f;
            }
        }
    }

    //Inicio cambio de la UI del menu
    public void changeUI()
    {
        cambiar = true;
        counting = true;
    }

    public void continuar()
    {
        Time.timeScale = 1f;
    }
    public void pausar()
    {
        Time.timeScale = 0;
    }
}
